import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class RegistrationPage {

    WebDriver driver;

    public RegistrationPage (WebDriver driver) {
        this.driver = driver;
    }

    public By registrationButton = By.xpath("//div[@title='Регистрация/Авторизация']//span[@class='header__actions-button-icon']");
    public By modalRegistrationButton = By.xpath("//button[contains(text(),'Регистрация')]");
    public By nameField = By.xpath("//input[@name='name']");
    public By surnameField = By.xpath("//input[@name='surname']");
    public By phoneNumberField = By.xpath("//body[1]/div[5]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/form[1]/div[4]/input[1]");
    public By emailField = By.xpath("//input[@name='email']");
    public By passwordField = By.xpath("//input[@name='password']");
    public By rePasswordField = By.xpath("//label[contains(text(),'Повторите пароль')]/preceding-sibling::input");
    public By privacyPolicyCheckbox = By.xpath("//label[@class='input-choice']/input");
    public By confirmRegistrationButton = By.xpath("//button[contains(text(),'Регистрация')]");
    public By warningMessage = By .xpath("//span[@class='form-error']");
    public By privateInformation = By.xpath("//h2[contains(text(),'Персональная информация')]");
    public By logOut = By.xpath("//span[@class='cabinet-menu__item-icon']/following-sibling::div[@class='cabinet-menu__item-text']");
    public By logOutModal = By.xpath("//h2[contains(text(), 'Возвращайтесь еще')]");
    public By logInModal = By.xpath("//h2[contains(text(), 'Вы успешно')]");
    public By logInEmailField = By.xpath("//input[@name='login']");
    public By logInPasswordField = By.xpath("//input[@name='password']");
    public By submitLogInButton = By.xpath("//button[contains(text(),'Войти')]");


    public void registration(String name, String surname, String phone, String email, String password, String rePassword) {
        Methods methods = new Methods(driver);
        methods.inputValue(driver, name, nameField);
        methods.inputValue(driver, surname, surnameField);
        methods.inputValue(driver, phone, phoneNumberField);
        methods.inputValue(driver, email, emailField);
        methods.waitMethMS(500);
        methods.inputValue(driver, password, passwordField);
        methods.inputValue(driver, password, rePasswordField);
    }

    public void logIn(String email, String password){
        Methods methods = new Methods(driver);
        methods.inputValue(driver, email, logInEmailField);
        methods.inputValue(driver, password, logInPasswordField);
        methods.clickElement(driver, submitLogInButton);
    }
}
