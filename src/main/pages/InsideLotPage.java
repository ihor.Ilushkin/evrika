import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class InsideLotPage {

    WebDriver driver;
     public InsideLotPage(WebDriver driver) {
         this.driver = driver;
     }

     public By insideLotItemDescription = By.xpath("//h1[@class='goods-title']");
     public By buyButton = By.xpath("//button[@class='button button_three button_fs_xl button_icon button_full']");
     public By basketTitle = By.xpath("//div[contains(text(), 'Корзина')]");
     public By orderButton = By.xpath("//a[@class='button button_one']");
     public By orderEmailField = By.xpath("//input[@type='email']");
     public By takeByYourSelfOption = By.xpath("//span[contains(text(),'Самовывоз из магазина')]");
     public By chooseShopDropdown = By.xpath("//div[@class='pickup__selected']");
     public By chooseShopList = By.xpath("//label[@class='pickup__option']");
     public By byCourier = By.xpath("//span[contains(text(),'Доставка курьером')]");
     public By locationDropdown = By.xpath("//div[@title='Алматы, Алматы']");
     public By locationList = By.xpath("//li[@class='select__option']");
     public By streetField = By.xpath("//label[contains(text(),'Улица')]/preceding-sibling::input");
     public By house = By.xpath("//label[contains(text(),'Дом')]/preceding-sibling::input");
     public By flatNumber = By.xpath("//label[contains(text(),'Квартира')]/preceding-sibling::input");
     public By datesDropdown = By.xpath("//div[@class='form-input-with-icon']");
     public By date = By.xpath("//span[@class='flatpickr-day ']");
     public By timeDropdown = By.xpath("//div[@title='Время']/ancestor::div[1]");
     public By timeList = By.xpath("//li[normalize-space()='11:00 - 15:00']");

     public By cashPayment = By.xpath("//span[contains(text(),'Наличными')]");
     public By payByCard = By.xpath("//span[contains(text(),'Картой онлайн')]");
     public By confirmOrderButton = By.xpath("//button[contains(text(),'Подтвердить заказ')]");
     public By middleNameField = By.xpath("//div[@class='checkout-step _mb-md']//div[3]//div[1]/input");
     public By priceInsideLot = By.xpath("//div[@class='_flex _flex-column-reverse _sm:flex-column']/div/div/div/div/div[1]");
     public By basketPrice = By.xpath("//div[@class='cart-resume__cost-value']");

     public void chooseShop(By locator, By locator1, int index) {
      Methods methods = new Methods(driver);
      methods.clickElement(driver, chooseShopDropdown);
      Methods.chooseItemFromTheList(driver, chooseShopList, index);
     }

     public void deliveryDestination(String street, String house, String flat) throws Exception {
      Methods methods = new Methods(driver);
      InsideLotPage insideLotPage = new InsideLotPage(driver);
      methods.inputValue(driver, street, insideLotPage.streetField);
      methods.inputValue(driver, house, insideLotPage.house);
      methods.inputValue(driver, flat, insideLotPage.flatNumber);
      methods.jsClick(insideLotPage.datesDropdown);
      methods.jsClick(insideLotPage.date);
      methods.jsClick(insideLotPage.timeDropdown);
      methods.jsClick(insideLotPage.timeList);
     }


}
