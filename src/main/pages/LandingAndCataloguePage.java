import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class LandingAndCataloguePage {

    WebDriver driver;

    public LandingAndCataloguePage(WebDriver driver) {
        this.driver = driver;
    }

    public By logo = By.xpath("//a[@class='header__logo-img']");
    public By smartphoneAndGadgetCategory = By.xpath("//span[@class='menu__link-name'][contains(text(),'Смартфоны и гаджеты')]");
    public By smartphones = By.xpath("//a[@class='menu__sub-link'][contains(text(),'Смартфоны')]");
    public By smartphonesLogo = By.xpath("//h1[contains(text(),'Смартфоны')]");
    public By listOfGoods = By.xpath("//div[@class='goods-tile__name']/a");
}
