import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;


import java.io.IOException;
import java.time.Duration;
import java.time.LocalTime;

public class usersTests {

    WebDriver driver;
    Methods methods;
    RegistrationPage registrationPage;
    LandingAndCataloguePage landingAndCataloguePage;
    InsideLotPage insideLotPage;

    @BeforeMethod
    public void setUp() throws Exception {
        driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(5));
        driver.manage().window().maximize();
        driver.get(Methods.getVariable("EvrikaURL"));
        methods = new Methods(driver);
        registrationPage = new RegistrationPage(driver);
        landingAndCataloguePage = new LandingAndCataloguePage(driver);
        insideLotPage = new InsideLotPage(driver);


    }

    @Test
    public void userCreationAndGoodsOrderUser1() throws IOException {
        //Registration, Log out, Log in
        methods.clickElement(driver, registrationPage.registrationButton);
        methods.clickElement(driver, registrationPage.modalRegistrationButton);
        String userEmail = methods.randomEmail();
        registrationPage.registration(methods.randomNameLastName(5), methods.randomNameLastName(5), methods.randomPhone(),
                userEmail, "password", "password");
        methods.waitForElementIsNotVisible(registrationPage.warningMessage, 20);
        methods.jsClick(registrationPage.privacyPolicyCheckbox);
        methods.jsClick(registrationPage.confirmRegistrationButton);
        methods.waitForElementIsVisible(registrationPage.privateInformation, 20);
        String time = LocalTime.now() + " " + "-screenshot01.png";
        String screenPath = "/Users/ihoriliushkin/IdeaProjects/evrika/screenshots/screenshot" + time;
        methods.takeScreenshot(driver, screenPath);
        String information = methods.getElementText(driver, registrationPage.privateInformation);
        Assert.assertEquals(information, "Персональная информация");

        //Log out
        methods.clickElement(driver, registrationPage.logOut);
        methods.waitForElementIsVisible(registrationPage.logOutModal, 20);
        String logOutModalText = methods.getElementText(driver, registrationPage.logOutModal);
        Assert.assertTrue(logOutModalText.contains("Возвращайтесь еще!"));

        //Log in
        methods.clickElement(driver, registrationPage.registrationButton);
        registrationPage.logIn(userEmail, "password");
        methods.waitForElementIsVisible(registrationPage.logInModal, 20);
        String logInModalText = methods.getElementText(driver, registrationPage.logInModal);
        String time1 = LocalTime.now() + " " + "-screenshot01.png";
        String screenPath1 = "/Users/ihoriliushkin/IdeaProjects/evrika/screenshots/screenshot" + time1;
        methods.takeScreenshot(driver, screenPath1);
        Assert.assertTrue(logInModalText.contains("Вы успешно авторизовались"));

        //Goods choosing
        methods.clickElement(driver, landingAndCataloguePage.logo);
        methods.hoverOverElement(driver, landingAndCataloguePage.smartphoneAndGadgetCategory);
        methods.waitForElementIsVisible(landingAndCataloguePage.smartphones, 20);
        methods.clickElement(driver, landingAndCataloguePage.smartphones);
        String smartphonesCategoryLogo = methods.getElementText(driver, landingAndCataloguePage.smartphonesLogo);
        Assert.assertEquals(smartphonesCategoryLogo, "Смартфоны");
        String catalogueLotDescription = Methods.chooseRandomListItem(driver, landingAndCataloguePage.listOfGoods);

        //Inside lot and order
        methods.waitForElementIsVisible(insideLotPage.insideLotItemDescription, 20);
        String insideLotDescription = methods.getElementText(driver, insideLotPage.insideLotItemDescription);
        Assert.assertEquals(catalogueLotDescription, insideLotDescription);

        String priceInsideLot = methods.getElementText(driver, insideLotPage.priceInsideLot);
        System.out.println(priceInsideLot);
        methods.clickElement(driver, insideLotPage.buyButton);
        methods.waitForElementIsVisible(insideLotPage.basketTitle, 20);
        String basketTitle = methods.getElementText(driver, insideLotPage.basketTitle);
        methods.waitForElementIsVisible(insideLotPage.basketPrice, 20);
        String basketPrice = methods.getElementText(driver, insideLotPage.basketPrice);
        System.out.println(basketPrice);
        Assert.assertTrue(basketTitle.contains("Корзина") && priceInsideLot.contains(basketPrice));

        methods.clickElement(driver, insideLotPage.orderButton);
        String middleName = methods.randomNameLastName(5);
        methods.inputValue(driver, middleName, insideLotPage.middleNameField);
        methods.clickElement(driver, insideLotPage.takeByYourSelfOption);
        insideLotPage.chooseShop(insideLotPage.chooseShopDropdown, insideLotPage.chooseShopList, 0);
        methods.clickElement(driver, insideLotPage.cashPayment);
        methods.clickElement(driver, insideLotPage.confirmOrderButton);
        //Error
    }

    //Summary: Error message 'Ошибка создания заказа' is appeared after order checkout
    //Description: Server responses: '"html":null, icon":"error'
    //Steps to reproduce:
    //1. Login as a user
    //2. Open Catalogue
    //3. Choose any lot
    //4. Click 'In basket' button -> Click 'Place an order button' -> Choose delivery - pick up and payment - cash
    //5. Click 'Confirm order' button
    //Expected Result: Lot is successfully ordered
    //Actual Result: Error message is appeared
    //Severity: Critical
    //Priority: Critical

    @Test
    public void userCreationAndGoodsOrderUser2() throws Exception {
        //Registration, Log out, Log in
        methods.clickElement(driver, registrationPage.registrationButton);
        methods.clickElement(driver, registrationPage.modalRegistrationButton);
        String userEmail = methods.randomEmail();
        registrationPage.registration(methods.randomNameLastName(5), methods.randomNameLastName(5), methods.randomPhone(),
                userEmail, "password", "password");
        methods.waitForElementIsNotVisible(registrationPage.warningMessage, 20);
        methods.jsClick(registrationPage.privacyPolicyCheckbox);
        methods.jsClick(registrationPage.confirmRegistrationButton);
        methods.waitForElementIsVisible(registrationPage.privateInformation, 20);
        String time = LocalTime.now() + " " + "-screenshot01.png";
        String screenPath = "/Users/ihoriliushkin/IdeaProjects/evrika/screenshots/screenshot" + time;
        methods.takeScreenshot(driver, screenPath);
        String information = methods.getElementText(driver, registrationPage.privateInformation);
        Assert.assertEquals(information, "Персональная информация");

        //Log out
        methods.clickElement(driver, registrationPage.logOut);
        methods.waitForElementIsVisible(registrationPage.logOutModal, 20);
        String logOutModalText = methods.getElementText(driver, registrationPage.logOutModal);
        Assert.assertTrue(logOutModalText.contains("Возвращайтесь еще!"));

        //Log in
        methods.clickElement(driver, registrationPage.registrationButton);
        registrationPage.logIn(userEmail, "password");
        methods.waitForElementIsVisible(registrationPage.logInModal, 20);
        String logInModalText = methods.getElementText(driver, registrationPage.logInModal);
        String time1 = LocalTime.now() + " " + "-screenshot01.png";
        String screenPath1 = "/Users/ihoriliushkin/IdeaProjects/evrika/screenshots/screenshot" + time1;
        methods.takeScreenshot(driver, screenPath1);
        Assert.assertTrue(logInModalText.contains("Вы успешно авторизовались"));

        //Goods choosing
        methods.clickElement(driver, landingAndCataloguePage.logo);
        methods.hoverOverElement(driver, landingAndCataloguePage.smartphoneAndGadgetCategory);
        methods.waitForElementIsVisible(landingAndCataloguePage.smartphones, 20);
        methods.clickElement(driver, landingAndCataloguePage.smartphones);
        String smartphonesCategoryLogo = methods.getElementText(driver, landingAndCataloguePage.smartphonesLogo);
        Assert.assertEquals(smartphonesCategoryLogo, "Смартфоны");
        String catalogueLotDescription = Methods.chooseRandomListItem(driver, landingAndCataloguePage.listOfGoods);

        //Inside lot and order
        methods.waitForElementIsVisible(insideLotPage.insideLotItemDescription, 20);
        String insideLotDescription = methods.getElementText(driver, insideLotPage.insideLotItemDescription);
        Assert.assertEquals(catalogueLotDescription, insideLotDescription);

        String priceInsideLot = methods.getElementText(driver, insideLotPage.priceInsideLot);
        System.out.println(priceInsideLot);
        methods.clickElement(driver, insideLotPage.buyButton);
        methods.waitForElementIsVisible(insideLotPage.basketTitle, 20);
        String basketTitle = methods.getElementText(driver, insideLotPage.basketTitle);
        methods.waitForElementIsVisible(insideLotPage.basketPrice, 20);
        String basketPrice = methods.getElementText(driver, insideLotPage.basketPrice);
        System.out.println(basketPrice);
        Assert.assertTrue(basketTitle.contains("Корзина") && priceInsideLot.contains(basketPrice));

        methods.clickElement(driver, insideLotPage.orderButton);
        String middleName = methods.randomNameLastName(5);
        methods.inputValue(driver, middleName, insideLotPage.middleNameField);
        methods.clickElement(driver, insideLotPage.byCourier);
        methods.waitForElementIsVisible(insideLotPage.streetField, 20);
        insideLotPage.deliveryDestination(Methods.getVariable("Street"), Methods.getVariable("House"), Methods.getVariable("Flat"));
        methods.waitMethMS(500);
        methods.clickElement(driver, insideLotPage.payByCard);
        methods.clickElement(driver, insideLotPage.confirmOrderButton);
        //Error
    }
    //Summary: Error message 'Ошибка создания заказа' is appeared after order checkout
    //Description: Server responses: '"html":null, icon":"error'
    //Steps to reproduce:
    //1. Login as a user
    //2. Open Catalogue
    //3. Choose any lot
    //4. Click 'In basket' button -> Click 'Place an order button' -> Choose delivery - courier and payment - card
    //5. Click 'Confirm order' button
    //Expected Result: Lot is successfully ordered
    //Actual Result: Error message is appeared
    //Severity: Critical
    //Priority: Critical

    @AfterMethod
    public void shutDown() {
        driver.quit();
    }

}
